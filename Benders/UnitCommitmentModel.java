
package Benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import problems.UnitCommitmentProblem;
 


public class UnitCommitmentModel {
    
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final UnitCommitmentProblem UCP;

    public UnitCommitmentModel(UnitCommitmentProblem UCP) throws IloException {
        this.UCP = UCP;
        this.model = new IloCplex();
        
        // Creates the decision variables 
        // 1. The u_gt variables
        u = new IloIntVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                u[g-1][t-1] = model.boolVar("u_"+g+"_"+t);
            }
        }
        
        // 2. The c_gt variables
        c = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c_"+g+"_"+t);
            }
        }
        
        // 3. The l_t variables
        l = new IloNumVar[UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            l[t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l_"+t);
        }
        
        
        // 4. The p_gt variables
        p = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p_"+g+"_"+t);
            }
        }
        
        
        // Creates the objective function
        // 1. Creates an empty linear expression
        IloLinearNumExpr obj = model.linearNumExpr();
        // 2. Adds the start-up costs, commitment costs and production costs
        
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                obj.addTerm(1, c[g-1][t-1]);    
                obj.addTerm(UCP.getCommitmentCost(g), u[g-1][t-1]);
                obj.addTerm(UCP.getProductionCost(g), p[g-1][t-1]);
            }
            // 3. Adds the shedding costs
            obj.addTerm(UCP.getSheddingCost(t), l[t-1]);
        }
        
        // 4. Adds the objective function to the model
        model.addMinimize(obj);
        
        
        // Creates the constraints
        
        // 1. Constraints (1b)
        // We have only constraint for each hour and generator
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][t-1]);
                // We bring the right-hand-side to the lhs, changing sign
                lhs.addTerm(-UCP.getStartUpCost(g), u[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(+UCP.getStartUpCost(g), u[g-1][t-2]); // Note that in order to get u_g,t-1 we need to access u[g-1][t-2] (notice the -2)                
                }
                // Finally we add the constraint
                model.addGe(lhs, 0);
            }
        }
        
        // 2. Constraints (1c)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= UCP.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
        
        // 2. Constraints (1d)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= UCP.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
        
        // 3. Constraints (1e)
        for(int t = 1; t <= UCP.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            model.addEq(lhs, UCP.getDemand(t));
        }
        
        // 4. Constraints (1f)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-UCP.getMinProduction(g), u[g-1][t-1]);
                model.addGe(lhs,0);
            }
        }
        
        // 5. Constraints (1g)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-UCP.getMaxProduction(g), u[g-1][t-1]);
                model.addLe(lhs,0);
            }
        }
        
        // 6. Constraints (1h)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                model.addLe(lhs,UCP.getRampUp(g));
            }
        }
        
        // 7. Constraints (1i)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                model.addLe(lhs,UCP.getRampDown(g));
            }
        }
        
        
    }
    
    public void solve() throws IloException{
        model.solve();
        System.out.println("Optimal objective from full model "+model.getObjValue());
    }
    
    public void printSolution() throws IloException{
        System.out.println("Commitment");
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            System.out.print(UCP.getGeneratorName(g));
            for(int t =1; t <= UCP.getnHours(); t++){
                System.out.print(String.format(" %4.0f ", model.getValue(u[g-1][t-1])));
            }
            System.out.println("");
        }
        System.out.println("Startup costs");
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            System.out.print(UCP.getGeneratorName(g));
            for(int t =1; t <= UCP.getnHours(); t++){
                System.out.print(String.format(" %4.2f ", model.getValue(c[g-1][t-1])));
            }
            System.out.println("");
        }
        System.out.println("Production");
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            System.out.print(UCP.getGeneratorName(g));
            for(int t =1; t <= UCP.getnHours(); t++){
                System.out.print(String.format(" %4.2f ", model.getValue(p[g-1][t-1])));
            }
            System.out.println("");
        }
    }
    
    
}
