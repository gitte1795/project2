/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Commons;

import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import Benders.UnitCommitmentModel;
import java.util.Locale;
import problems.MasterProblem;
import problems.UnitCommitmentProblem;


public class Main {
    public static void main(String[] args) throws FileNotFoundException, IloException{
   
        // 1. READS THE GENERATORS DATA
        // The generators data are stored in the generators.txt file
       
        File generatorsFile = new File("generators.txt");
        Scanner gi = new Scanner(generatorsFile).useLocale(Locale.US);
        
        // We skip the first two lines of the file since they are a header
        // That is, we move the cursor past the first two lines (at the beginning of the third)
        gi.nextLine();
        gi.nextLine();
        
        // We create the arrays for storing the data we read.
        // These arrays will then be used to create an instance of 
        // the UnitCommitmentProblem class.
        
        // We have 31 generators in the file
        int nGenerators = 31;
        String[] names = new String[nGenerators];
        double[] minProduction = new double[nGenerators];
        double[] maxProduction = new double[nGenerators];
        double[] startUpCosts = new double[nGenerators];
        double[] commitmentCosts = new double[nGenerators];
        double[] rampUp = new double[nGenerators];
        double[] rampDown = new double[nGenerators];
        int[] minUpTime = new int[nGenerators];
        int[] minDownTime = new int[nGenerators];
        double[] productionCost = new double[nGenerators];
        
        for(int i = 1; i <= nGenerators; i++){
            names[i-1] = gi.next();
            minProduction[i-1] = gi.nextDouble();
            maxProduction[i-1] = gi.nextDouble();
            startUpCosts[i-1] = gi.nextDouble();
            commitmentCosts[i-1] = gi.nextDouble();
            rampUp[i-1] = gi.nextDouble();
            // RampDown is equal to RampUp
            rampDown[i-1] = rampUp[i-1];
            minUpTime[i-1] = gi.nextInt();
            minDownTime[i-1] = gi.nextInt();
            productionCost[i-1] = gi.nextDouble();
        }
        
        //We print to check if we have read in the txt file correctly.  
        for(int g = 1; g <= nGenerators; g++){
            System.out.println("Names ="+names[g-1]+"   "+"minProduction = "+minProduction[g-1]+"   "+"maxProduction = "+maxProduction[g-1]+
                    "   "+"startUpCosts = "+startUpCosts[g-1]+"   "+"commitmentCosts = "+commitmentCosts[g-1]+"   "+"rampUp = "+rampUp[g-1]+
                    "   "+"rampDown = "+rampDown[g-1]+"   "+"minUpTime = "+minUpTime[g-1]+"   "+"minDownTime = "+minDownTime[g-1]+
                    "   "+"productionCost = "+productionCost[g-1]);
        }
        
        
        // 2. READS THE LOADS DATA
        // We have 24 hours
        int nHours = 24;
        double[] demand = new double[nHours];
        
        File loadsFile = new File("loads.txt");
        Scanner s = new Scanner(loadsFile).useLocale(Locale.US);
        
        s.nextLine();
        
        for(int t = 1; t <= nHours; t++){
            demand[t-1] = s.nextDouble();
        
            System.out.println("demand = "+demand[t-1]);
        }
        
        // We calculate min up time and down time for each t
        int[][] minUpTimeAtT = new int[nGenerators][nHours];
        int[][] minDownTimeAtT = new int[nGenerators][nHours];
        for(int g = 1; g <= nGenerators; g++){
            for(int t = 1; t <= nHours; t++){
                minUpTimeAtT[g-1][t-1] = Math.min(t+minUpTime[g-1]-1, nHours);
                minUpTimeAtT[g-1][t-1] = Math.min(t+minDownTime[g-1]-1, nHours);
            }
        }
        
                
        // We calculate the highest production cost, and set the shedding cost
        // twice as high as the highest production cost
        double[] sheddingCosts = new double[nHours];
        double max = 0;
        for(int g = 1; g <= nGenerators; g++){
            if (productionCost[g-1] >= max){
                max = productionCost[g-1];
            }
        }
        
        for(int t = 1; t <= nHours; t++){
            sheddingCosts[t-1] = 2*max;
        }
            
        
        // 3. CREATES AN INSTANCE OF THE UNIT COMMITMENT PROBLEM
        
        UnitCommitmentProblem UCP 
                = new UnitCommitmentProblem(nHours,nGenerators,
                        names,commitmentCosts,productionCost,
                        sheddingCosts, startUpCosts,
                        minUpTime,minDownTime,
                        minUpTimeAtT,minDownTimeAtT,
                        minProduction,maxProduction,
                        rampUp,rampDown,
                        demand
        );
        
        // 4. CREATES THE MATHEMATICAL MODEL AND SOLVES IT (WITHOUT DECOMPOSITION)
        //UnitCommitmentModel UCPm = new UnitCommitmentModel(UCP);
        //UCPm.solve();
        //UCPm.printSolution();
        
        
        // 5. SOLVES THE PROBLEM USING BENDERS DECOMPOSITION
        // Creates the Master Problem
        MasterProblem MP = new MasterProblem(UCP);
        MP.solve();
        System.out.println("Optimal Benders objective value = "+MP.getObjective());
        MP.printSolution();
        
        // Checks if the results are consistent with solving the UCM problem
        UnitCommitmentModel UCM = new UnitCommitmentModel(UCP);
        UCM.solve();
        
        
        System.out.println("Optimal Benders objective value = "+MP.getObjective());
        //System.out.println("Optimal full model objective value = "+UCM.getobjvalue() ;

    }
    

}
