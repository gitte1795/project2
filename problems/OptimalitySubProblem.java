
package problems;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;


public class OptimalitySubProblem {
    private final IloCplex model;
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final UnitCommitmentProblem UCP;
    // Then we define the constraints:
    private final IloRange Const1e[];
    private final IloRange Const1f[][];
    private final IloRange Const1g[][];
    private final IloRange Const1h[][];
    private final IloRange Const1i[][];

    /**
     * We will now create the LP model for the optimality subproblem
     * @param UCP
     * @param U a solution to MP
     * @throws IloException 
     */
    
    public OptimalitySubProblem(UnitCommitmentProblem UCP, double U[][]) throws IloException {
        this.UCP = UCP;
        
        // 1. We start by creating an IloCplex object
        this.model = new IloCplex();
        
        // 2. Then we creates the decision variables, which is p_gt and l_t
        // The same may as in the feasibility subproblem
        p = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        l = new IloNumVar[UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        // 3. Now we can create the objective function
        // We start by creating an empty linear numerical expression as in MP
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // And then we add terms to the equation of the objective function. 
        /* 
        * The objective function in the optimality subproblem is the rest of
        * original objetive function which was contained in phi in MP.
        */ 
        
        for(int t = 1; t <= UCP.getnHours(); t++){
            obj.addTerm(UCP.getSheddingCost(t), l[t-1]);
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                obj.addTerm(UCP.getProductionCost(g), p[g-1][t-1]);
            }
        }
        
        //The objective function is to be minimized, so we tell cplex to do that
        model.addMinimize(obj);
        
         // 4. The next step is to create the constraints
        /*
        * Like in the feasibility subproblem we create a linear equation, 
        * IloLinearNumExpr, for each constraints. The constraints we have to 
        * add are the ones that are not in the master problem.
        * The constraints is found in the UnitCommitmentModel. 
        */
        
        // 3. Constraints (1e)
        this.Const1e = new IloRange[UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            Const1e[t-1] = model.addEq(lhs, UCP.getDemand(t));
        }
        
        // 4. Constraints (1f)
        this.Const1f = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Const1f[g-1][t-1] = model.addGe(lhs, UCP.getMinProduction(g)*U[g-1][t-1]);
                //Notice that we use U_gt and ont u_gt, this is for the same 
                // reason as in the feasibility subproblem. 
            }
        }
        
        // 5. Constraints (1g)
        this.Const1g = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                Const1g[g-1][t-1] = model.addLe(lhs, UCP.getMaxProduction(g)*U[g-1][t-1]);
            }
        }
        
        // 6. Constraints (1h)
        this.Const1h = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                // The same way as in UniCommitmentModel we defined the 
                // p_gt and p_g(t-1)
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                Const1h[g-1][t-1] = model.addLe(lhs, UCP.getRampUp(g));
            }
        }
        
        // 7. Constraints (1i)
        this.Const1i = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,p[g-1][t-1]);
                Const1i[g-1][t-1] = model.addLe(lhs, UCP.getRampDown(g));
            }
        }
    }
    
    /**
     * We solve the optimality subproblem
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    /**
     * Then we return the objective value of the optimality subproblem
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /*
     * If the solution is not optimal we want to add a optimality cut. 
     * This is done in the same way as in the feasibility subproblem:
     * by taking the dual variables of the constraints. 
     * We can get the dual variables with model.getDual() and then multiply it
     * with the right-hand-side of the current constraint. 
     * Then we sum over the conditions of the dual varibales. This means that we
     * sum over g and t (or just t for (1e)), set it less or equal to phi, 
     * which creates the cut.
     * First we create the constant part of the cut (the part that does not 
     * denpend on u_gt) and after we create the linar (the part that depend on 
     * u_gt) part of the cut
     *
     * @return the constant of the cut
     * @throws IloException 
     */
    
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= UCP.getnHours(); t++){
            constant = constant + model.getDual(Const1e[t-1]) * UCP.getDemand(t);
            for(int g = 1; g <= UCP.getnGenerators(); g++){
            constant = constant + model.getDual(Const1h[g-1][t-1]) * UCP.getRampUp(g) + 
                    model.getDual(Const1i[g-1][t-1]) * UCP.getRampDown(g);
            }
        }
        return constant;
    }
    
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                cutTerm.addTerm(model.getDual(Const1f[g-1][t-1]) * UCP.getMinProduction(g), u[g-1][t-1]); 
                cutTerm.addTerm(model.getDual(Const1g[g-1][t-1]) * UCP.getMaxProduction(g), u[g-1][t-1]); 
            }
        }
        return cutTerm; 
    }
    
    // We release all the objects retained by the IloCplex object.
    public void end(){
        model.end();
    }
        
        
    
}
