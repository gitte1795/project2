
package problems;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;


public class FeasibilitySubProblem {
    private final IloCplex model;
    private final IloNumVar p[][];
    private final IloNumVar l[];
    // We define the slack variabes:
    private final IloNumVar Sminus[];
    private final IloNumVar Jplus[][];
    private final IloNumVar Iminus[][];
    private final IloNumVar Eminus[][];
    private final IloNumVar Aminus[][];
    private final UnitCommitmentProblem UCP;
    // Then we define the constraints:
    private final IloRange Const1e[];
    private final IloRange Const1f[][];
    private final IloRange Const1g[][];
    private final IloRange Const1h[][];
    private final IloRange Const1i[][];

    
    /**
     * We will now create the LP model for the feasibility subproblem
     * @param UCP
     * @param U a solution to MP
     * @throws IloException 
     */
    
    public FeasibilitySubProblem(UnitCommitmentProblem UCP, double U[][]) throws IloException {
        this.UCP = UCP;
        
        // 1. We start by creating an IloCplex object
        this.model = new IloCplex();
        
        // 2. Then we creates the decision variables, which is p_gt and l_t
        // We use the method IloNumVat since the variables are continous. 
        p = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                p[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        l = new IloNumVar[UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            l[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        // Then we create the slack variables. 
        // We define them the same way as the variables, from zero to infinity. 
        
        this.Sminus = new IloNumVar[UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            Sminus[t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
        }
        
        this.Jplus = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                Jplus[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.Iminus = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                Iminus[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.Eminus = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                Eminus[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        this.Aminus = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                Aminus[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        
        // 3. Now we can create the objective function
        // We start by creating an empty linear numerical expression as in MP
        IloLinearNumExpr obj = model.linearNumExpr();

        // And then we add terms to the equation of the objective function. 
        /* 
        * The objective function in the feasibility subproblem is the sum of
        * all the slack variables.
        */ 
        
        // We start with the first sum, which is only over t:
        for(int t = 1; t <= UCP.getnHours(); t++){
            obj.addTerm(1, Sminus[t-1]);
        }
        //Then we take the rest, which is both over g and t
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                obj.addTerm(1, Jplus[g-1][t-1]);
                obj.addTerm(1, Iminus[g-1][t-1]);
                obj.addTerm(1, Eminus[g-1][t-1]);
                obj.addTerm(1, Aminus[g-1][t-1]);
            }
        }
        
        //The objective function is to be minimized, so we tell cplex to do that
        model.addMinimize(obj);
        
        
        // 4. The next step is to create the constraints
        /*
        * We create a linear equation, IloLinearNumExpr, for each constraints  
        * in the feasibility subproblem. The constraints we have to add are the
        * ones that are not in the master problem. We add the slack 
        * variables to the constraints. 
        */
       
        // Constarint (1e)
        /*
        * Since the constraint contains the variable l_t which is non-negative 
        * and continous, it will work like the positive variable, and therefore
        * we only have to add Sminus and not Splus (which we have not defined). 
        *
        * The constraints are found in the UnitCommitmentModel, the difference 
        * is that  we add the slack variables. 
        */ 
       
        this.Const1e = new IloRange[UCP.getnHours()]; 
        for(int t = 1; t <= UCP.getnHours(); t++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                lhs.addTerm(1, p[g-1][t-1]);
            }
            lhs.addTerm(1, l[t-1]);
            lhs.addTerm(-1, Sminus[t-1]);
            Const1e[t-1] = model.addEq(lhs, UCP.getDemand(t));
        }
        
        // Constraints (1f)
        this.Const1f = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(1, Jplus[g-1][t-1]);
                Const1f[g-1][t-1] = model.addGe(lhs, UCP.getMinProduction(g)*U[g-1][t-1]);
            // We use the U_gt instead of u_gt, since U_gt is the solution in
            // the current node when we branch and bound. 
            }
        }
        
        // Constraints (1g)
        this.Const1g = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1,p[g-1][t-1]);
                lhs.addTerm(-1, Iminus[g-1][t-1]);
                Const1g[g-1][t-1] = model.addLe(lhs, UCP.getMaxProduction(g)*U[g-1][t-1]);
            // For the same reason we use U_gt in this constraint. 
            }
        }
        
        // Constraints (1h)
        this.Const1h = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // The same way as in UniCommitmentModel we defined the 
                // p_gt and p_g(t-1)
                lhs.addTerm(1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(-1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,Eminus[g-1][t-1]);
                Const1h[g-1][t-1] = model.addLe(lhs, UCP.getRampUp(g));
            }
        }
        
        // Constraints (1i)
        this.Const1i = new IloRange[UCP.getnGenerators()][UCP.getnHours()];
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(-1,p[g-1][t-1]);
                if(t > 1){
                    lhs.addTerm(1,p[g-1][t-2]);
                }
                lhs.addTerm(-1,Aminus[g-1][t-1]); 
                Const1i[g-1][t-1] = model.addLe(lhs, UCP.getRampDown(g));
            }
        }
    
    }
    /**
     * We solve the feasibility subproblem
     * @throws IloException 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    /**
     * Then we return the objective value of the feasibility subproblem
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /*
     * If the solution is not feasible we want to add a feasibility cut. 
     * This is done by taking the dual variables of the constraints. 
     * We can get the dual variables with model.getDual() and then multiply it
     * with the right-hand-side of the current constraint. 
     * Then we sum over the conditions of the dual varibales. This means that we
     * sum over g and t (or just t for (1e)), set it less or equal to zero, 
     * which creates the cut.
     * First we create the constant part of the cut (the part that does not 
     * denpend on u_gt) and after we create the linar (the part that depend on 
     * u_gt) part of the cut
     *
     * @return the constant of the cut
     * @throws IloException 
     */
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int t = 1; t <= UCP.getnHours(); t++){
            constant = constant + model.getDual(Const1e[t-1]) * UCP.getDemand(t-1);
            for(int g = 1; g <= UCP.getnGenerators(); g++){
            constant = constant + model.getDual(Const1h[g-1][t-1]) * UCP.getRampUp(g-1) + 
                    model.getDual(Const1i[g-1][t-1]) * UCP.getRampDown(g-1);
            }
        }
        return constant;
    }
    
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int t = 1; t <= UCP.getnGenerators(); t++){
            for(int g = 1; g <= UCP.getnHours(); g++){
                cutTerm.addTerm(model.getDual(Const1f[g-1][t-1]) * UCP.getMinProduction(g-1), u[g-1][t-1]); 
                cutTerm.addTerm(model.getDual(Const1g[g-1][t-1]) * UCP.getMaxProduction(g-1), u[g-1][t-1]); 
            }
        }
        return cutTerm;
    }
    
    // We release all the objects retained by the IloCplex object.
    public void end(){
        model.end();
    }
  
}
