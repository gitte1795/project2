
package problems;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;


public class MasterProblem {
    private final IloCplex model;
    private final IloNumVar c[][];
    private final IloIntVar u[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem UCP;
    
    /**
     * Creates the Master Problem.
     * Where we use the UnitCommitmentProblem.
     * @param UCP
     * @throws IloException 
     */
    
    /*
    * In Benders Decomposition, the first step is to formulate the MP.
    * To formulate the Master Problem we spot which variables that are
    * complicating. In this problem the complicating variable is u_gt, since 
    * it is binary and without this it would be a linear LP, and nicely solved.
    *
    * We decide to include c_gt in the MP because it is only appearing in
    * constraint (1b) and the objective function.
    */
    
    public MasterProblem(UnitCommitmentProblem UCP) throws IloException {
        this.UCP = UCP;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        /*
        * We have two decision variables, c_gt and u_gt, in the master problem. 
        * we start with c_gt which is a non-negative variable, and therefore it should
        * be an IloVar. Each position can take the values from zero to infinity.        
        */
        this.c = new IloNumVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                c[g-1][t-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }

        /*
        * Then u_gt which is a binary variable, and should be an IloIntVar and 
        * each position should be a boolVar.
        */
        this.u = new IloIntVar[UCP.getnGenerators()][UCP.getnHours()];
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                u[g-1][t-1] = model.boolVar();
            }
        }
        /*
        * We create phi which containts the rest of the objective function from 
        * the original problem
        */
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");   
    
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        
        // Then we adds terms to the equation 
        /*
        * We add the term from the objetive function in the original problem,
        * which is the double sum for g and t
        */
        for(int g = 1; g<= UCP.getnGenerators(); g++){
            for(int t = 1; t <= UCP.getnHours(); t++){
                obj.addTerm(1, c[g-1][t-1]);
                obj.addTerm(UCP.getCommitmentCost(g), u[g-1][t-1]);
            }
        }
    // Now we add the term phi to the objective function
    obj.addTerm(1, phi);
        
    // We tells cplex to minimize the objective function
    model.addMinimize(obj);
        
    /**
    * Then we add the constraints to the master problem.
    * We add the constraints by creating linear equation with the method
    * IloLinearNumExpr. 
    * We include constraint (1b), (1c), (1d), (1j) and (1k) from the original
    * problem. Since these constraints only contain u_gt and c_gt
    * Constraint (1j) and (1k) are already defined when we created
    * the variables. 
    */
    
    // Constraint (1b) 
    /*
    * Notice that the constraint are already defined in the UnitCommitmentModel. 
    */
    for(int t = 1; t <= UCP.getnHours(); t++){
        for(int g = 1; g <= UCP.getnGenerators(); g++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            lhs.addTerm(1, c[g-1][t-1]);
            // The same way as in UniCommitmentModel we defined the 
                // u_gt and u_g(t-1)
            lhs.addTerm(-UCP.getStartUpCost(g), u[g-1][t-1]);
            if(t > 1){
                lhs.addTerm(+UCP.getStartUpCost(g), u[g-1][t-2]);                
            }
            model.addGe(lhs, 0);
        }
    }
    //Constraint (1c)
    for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                for(int tt = t; tt <= UCP.getMinOnTimeAtT(g, t); tt++){
                    lhs.addTerm(1, u[g-1][tt-1]);
                    lhs.addTerm(-1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
    
    // 2. Constraints (1d)
        for(int t = 1; t <= UCP.getnHours(); t++){
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over t'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int tt = t; tt <= UCP.getMinOffTimeAtT(g, t); tt++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][tt-1]);
                    lhs.addTerm(1, u[g-1][t-1]);
                    if(t > 1){
                        lhs.addTerm(-1, u[g-1][t-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
    
    }
    // We tells Cplex to solve the Master Problem.
        public void solve() throws IloException{    
        // In this way we inform Cplex that
        // we want to use the callback we define below, which is an object, and is only visible in the master problem.
  
        model.use(new Callback());
        
        // Solve the problem
        model.solve();
    }
    
        /*
        * We want to use the callback which we define below, and by doing this
        * we tell Cplex to solve the problem.
        *
        * The class Callback extends the LazyConstraintCallback, and this means  
        * that we can add constraints to the problem while the problem it solved 
        * with branch and bound. 
        * the reason why we are using this class, is beacuse we add the 
        * feasibility cuts if the solution is non feasible and optimality cuts 
        * when the solution is not optimal. This is a part of the Benders 
        * Decomposition.
        */
        
    // We create the callback class. 
    private class Callback extends IloCplex.LazyConstraintCallback{
 
        public Callback() {
        }
        /**
         * This is the main method of the Callback class.
         * Whatever we code in this method will be run every time
         * the B&B method reaches an integer node. Here we 
         * code the routines necessary to verify whether we need
         * cuts and, in case, generate and add cuts. 
         * @throws IloException 
         */

        // We use @Override since we are changing the solve method. 
        @Override
        protected void main() throws IloException {
            // 1. We start by obtaining the solution at the current node, 
            // We do this to check if the solution is feasible and/or optimal.
            double[][] U = getU();
            double Phi = getPhi();
            
            // 2. We check feasibility of the subproblem
            /*
            * This is the next step of the Benders decomposition. We do this 
            * by solving the feasibility subproblem, which is created in a new 
            * class in another file. 
            * c_gt it not in the feasibility subproblem and 
            * therefore we only consider u_gt and phi. 
            */
            
            // 2.1 We create and solve a feasibility subproblem 
            FeasibilitySubProblem FSP = new FeasibilitySubProblem(UCP, U);
            FSP.solve();
            double fspObjective = FSP.getObjective();
         
            // 2.2 We check if the suproblem is feasible. 
            /*
            * We remember that the subproblem is feasible if the objective value
            * is zero. 
            * If the value is positive we make a feasibility cuts, since the 
            * solution to the feasibility subproblem is not feasible in the 
            * master problem. 
            * The cuts we add will be LinearTerm <= -constantTerm, and is added 
            * by using the method add(). 
            */

            System.out.println("FSP "+fspObjective);   
            if(fspObjective >= 0+1e-9){
                // 2.3 If the objective is positive the solution to the
                // subproblem is not feasible, and we add a feasibility cut.
                System.out.println("Generating feasibility cut");
                // 2.4 We obtain the constant and the linear term of the cut
                // from the feasibility subproblem
                double constant = FSP.getCutConstant();
                IloLinearNumExpr linearTerm = FSP.getCutLinearTerm(u);           
                add(model.le(linearTerm, -constant));
            }else{
            
                // 3. Then we have a feasible subproblem, we check optimality
                /* 
                * if the objetive value is not optimal to the problem we add 
                * an optimality cut. But first we create the class optimality 
                * subproblem the same way as the feasibility sub problem and 
                * tells cplex to solve it.
                */
                // 3.1. First, we create and solve an optimality suproblem
            
                OptimalitySubProblem OSP = new OptimalitySubProblem(UCP,U);
                OSP.solve();
                double ospObjective = OSP.getObjective();
                
                // 3.2. Then we print out the value of phi to check if the value
                // is optimal. The solution is optimal if phi >= objectiveValue
                // if not we add a optimality cut. 
            
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // 3.3. If this holds the problem at the current node
                    // is optimal.
                    System.out.println("The current node is optimal");
                }else{
            // 3.4. If the first statement does not hold we need an optimality 
            // cut.  
                    System.out.println("Generating optimality cut");
                    // In this print we get the constant and the linear term 
                    // from the optimality suproblem 
                    double cutConstant = OSP.getCutConstant();
                    IloLinearNumExpr cutTerm = OSP.getCutLinearTerm(u);
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut. 
                    add(model.le(cutTerm, -cutConstant));
                }
            }
        }
        /*
        * We return the U and the phi component of the solution at the current 
        * branch and bound integer node. We do this since we need it in the 
        * Benders Decomposition. 
        * We start which the U component:
        * @return the U component of the current solution
        * @throws IloException 
        */
        public double[][] getU() throws IloException{
            double U[][] = new double[UCP.getnGenerators()][UCP.getnHours()];
            for(int g = 1; g <= UCP.getnGenerators(); g++){
                for(int t = 1; t <= UCP.getnHours(); t++){
                    U[g-1][t-1] = getValue(u[g-1][t-1]);
                }
            }
           return U;
       }
        
        /*
        * Then we returns the value of phi at the current branch and bound node.
        * @return the value of phi.
        * @throws IloException 
        */
        public double getPhi() throws IloException{
           return getValue(phi);
        }
    }
    
    /*
     * We returns the objective value. 
     * @throws IloException 
     * and after we print out the solution of the Benders Decomposition. 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    public void printSolution() throws IloException{
         for(int g = 1;g <= UCP.getnGenerators(); g++){
                for(int t = 1; t <= UCP.getnHours(); t++){ 
            System.out.println( "Generator "+UCP.getGeneratorName(g)+ " in hour " +t+ " = "+model.getValue(u[g-1][t-1]));
            }
        }   
    }
    
    // We release all the objects retained by the IloCplex object.
    public void end(){
        model.end();
    }
       
                
}


